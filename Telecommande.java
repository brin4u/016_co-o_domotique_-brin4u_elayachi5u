import java.util.ArrayList;

public class Telecommande {
	private ArrayList<Machines> television;
	
	
	public Telecommande(){
		this.television=new ArrayList<Machines>();
	}
	
	public void ajouterMachine(Machines l){
		this.television.add(l);
	}
	
	public void activerMachine(int i){
		i--;
		Machines p=this.television.get(i);
		p.allumer();
		
	}
	
	public void desactiverMachine(int i){
		i--;
		Machines p=this.television.get(i);
		p.eteindre();	
	}
	
	public void activerTout(){
		for(Machines n :this.television){
			n.allumer();
		}
		
	}
	public String toString(){
		String res="la telecomande controle :";
		for(Machines n :this.television){
			res=res+" ;"+n.toString();
		}
		return res;
	}
	
	
	
}
